<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/logged" method="post">
      @csrf
      <label for="">First Name:</label> <br /><br />
      <input type="text" name="fname" id="name" /> <br /><br />

      <label for="">Last Name:</label> <br /><br />
      <input type="text" name="lname" id="" /><br /><br />

      <label for="">Gender:</label> <br /><br />
      <input type="radio" name="gender" id="male" /> Male <br />
      <input type="radio" name="gender" id="female" /> Female <br />
      <input type="radio" name="gender" id="other" /> Other <br /><br />

      <label for="">Nationality:</label><br /><br />
      <select name="Nationality" id="national">
        <option value="Indonesian">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
      </select>
      <br /><br />

      <label for="Language">Language Spoken:</label> <br /><br />
      <input type="checkbox" name="Bahasa Indonesia" id="Bahasa Indonesia" /> Bahasa Indonesia <br />
      <input type="checkbox" name="English" id="English" /> English <br />
      <input type="checkbox" name="Other" id="Other Language" /> Other <br /><br />

      <label for="Bio">Bio:</label> <br /><br />
      <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea>
      <br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
