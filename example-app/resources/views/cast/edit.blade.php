@extends('layouts.master')

@section('title')
    Halaman Edit Cast
@endsection
 
@section('sub-title')
    Halaman Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method("put")
    <div class="form-group">
      <label >Cast Name</label>
      <input type="text" name= "nama" value ="{{$cast->nama}}" class="form-control">

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value ="{{$cast->umur}}" class="form-control" >
    </div>
    
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class= "form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
      </div>

    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection