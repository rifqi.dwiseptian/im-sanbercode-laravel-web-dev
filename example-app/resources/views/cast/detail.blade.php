@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection
 
@section('sub-title')
    Halaman Cast
@endsection

@section('content')


<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
@endsection