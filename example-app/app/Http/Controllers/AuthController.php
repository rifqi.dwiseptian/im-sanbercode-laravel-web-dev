<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('page.form');
    }
    public function send(Request $request){
        $firstname = $request['fname'];
        $lastname = $request['lname'];

        return view('page.welcome', ['firstname'=> $firstname,'lastname'=> $lastname]);
    }
}
