<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'register']);
Route::get('/form', [AuthController::class, 'form']);

Route::post('/logged', [AuthController::class, 'send']);

Route::get('/data-table', function(){
    return view('page.data-table');
});

Route::get('/table', function(){
    return view('page.table');
});


//CRUD category
//Create Data
//rute untuk mengarah ke halaman form input kategori
Route::get('/cast/create', [CastController::class, 'create']);

//rute untuk memasukkan inputan ke database
Route::post('/cast', [CastController::class, 'store']);

//rute untuk mengambil semua data di database 
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//update data
Route::get("/cast/{cast_id}/edit", [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


